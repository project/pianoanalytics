/**
 * Creates and AT Internet Piano Analytics and dispatches it.
 */
(($, Drupal, once, drupalSettings) => {
  // Process only if pianoanalytics settings have been defined.
  if (drupalSettings.pianoanalytics === undefined) {
    return;
  }

  Drupal.pianoanalytics = {
    _initialize(settings) {
      const paConfigurations = {};
      if (settings.site) {
        paConfigurations.site = settings.site;
      }
      if (settings.collect_domain) {
        paConfigurations.collectDomain = settings.collect_domain;
      }
      if (settings.add_event_url) {
        paConfigurations.addEventURL = settings.add_event_url;
      }
      window.pa.setConfigurations(paConfigurations);
    },

    _configurePrivacy(settings) {
      if (window.pa.privacy) {
        if (settings.cnil_exempt !== 0) {
          window.pa.privacy.setMode('exempt');
        }
      }
    },

    _addChaptersToProperties(settings, properties, prefix) {
      if (settings.page_chapter1) {
        properties[`${prefix}_chapter1`] = settings.page_chapter1;
      }
      if (settings.page_chapter2) {
        properties[`${prefix}_chapter2`] = settings.page_chapter2;
      }
      if (settings.page_chapter3) {
        properties[`${prefix}_chapter3`] = settings.page_chapter3;
      }
    },

    _processPage(settings) {
      const pageProperties = {};
      if (settings.page_name) {
        pageProperties.page = settings.page_name;
      }
      this._addChaptersToProperties(settings, pageProperties, 'page');
      if (Object.getOwnPropertyNames(pageProperties).length > 0) {
        window.pa.sendEvent('page.display', pageProperties);
      }
    },

    _registerActionListeners() {
      const self = this;
      // Attach mousedown, keyup, touchstart events to document only and catch
      // clicks on all elements.
      $(document.body).on('mousedown keyup touchstart', (event) => {
        // Process only if the pianoanalytics library has been properly loaded.
        if (window.pa === undefined) {
          return;
        }

        // Catch the closest surrounding link of a clicked element.
        $(event.target)
          .closest('a,area')
          .each(function processLinks() {
            const link = this;
            // Is the clicked URL internal?
            if (Drupal.pianoanalytics.isInternal(this.href)) {
              // Is download tracking activated and the file extension configured
              // for download tracking?
              if (
                drupalSettings.pianoanalytics.trackDownload &&
                Drupal.pianoanalytics.isDownload(link.href)
              ) {
                // Download link clicked.
                const clickProperties = { click: link.href };
                Drupal.pianoanalytics._addChaptersToProperties(
                  drupalSettings.pianoanalytics,
                  clickProperties,
                  'click',
                );
                window.pa.sendEvent('click.download', clickProperties);
              }
            } else if (
              drupalSettings.pianoanalytics.trackMailto &&
              link.matches("a[href^='mailto:'],area[href^='mailto:']")
            ) {
              // Mailto link clicked.
              const clickProperties = { click: link.href };
              self._addChaptersToProperties(
                drupalSettings.pianoanalytics,
                clickProperties,
                'click',
              );
              window.pa.sendEvent('click.action', clickProperties);
            } else if (
              drupalSettings.pianoanalytics.trackOutbound &&
              this.href.match(/^\w+:\/\//i)
            ) {
              // External link clicked / No top-level cross-domain clicked.
              const clickProperties = { click: link.href };
              self._addChaptersToProperties(
                drupalSettings.pianoanalytics,
                clickProperties,
                'click',
              );
              window.pa.sendEvent('click.navigation', clickProperties);
            }
          });
      });
    },

    _triggerInitializedEvent() {
      $(document).trigger('pianoanalytics:tag_initialized');
    },

    initializeAndTrackPage(settings = drupalSettings.pianoanalytics) {
      this._initialize(settings);
      this._configurePrivacy(settings);
      this._processPage(settings);
      this._registerActionListeners();
      this._triggerInitializedEvent();
    },

    /**
     * Check whether this is a download URL or not.
     *
     * @param {string} url
     *   The web url to check.
     *
     * @return {boolean} isDownload
     */
    isDownload(url) {
      const isDownload = new RegExp(
        `\\.(${drupalSettings.pianoanalytics.trackDownloadExtensions})([?#].*)?$`,
        'i',
      );
      return isDownload.test(url);
    },

    /**
     * Check whether this is an absolute internal URL or not.
     *
     * @param {string} url
     *   The web url to check.
     *
     * @return {boolean} isInternal
     */
    isInternal(url) {
      const isInternal = new RegExp(`^(https?)://${window.location.host}`, 'i');
      return isInternal.test(url);
    },
  };

  Drupal.behaviors.pianoanalytics = {
    attach(context, settings) {
      once('pianoanalytics', 'html', context).forEach(() => {
        // Process only if the pianoanalytics library has been properly loaded.
        if (window.pa === undefined) {
          return;
        }
        Drupal.pianoanalytics.initializeAndTrackPage(settings.pianoanalytics);
      });
    },
  };
})(jQuery, Drupal, once, drupalSettings);
