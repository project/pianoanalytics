<?php

namespace Drupal\pianoanalytics;

/**
 * Various utility functions for the Piano Analytics module.
 */
class PianoAnalyticsUtils {

  /**
   * Define the default file extension list that should be tracked as download.
   */
  const TRACKFILES_EXTENSIONS = '7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip';

  /**
   * Builds library info using contributed script url.
   */
  public static function getLibraryInfoFromUrl($url) {
    $library_info = [
      // Load script in header.
      'header' => TRUE,
      'js' => [
        $url => [
          // Is an external URL.
          'external' => TRUE,
        ],
      ],
    ];
    return $library_info;
  }

  /**
   * Builds library info using uploaded script file.
   */
  public static function getLibraryInfoFromFile($file) {
    $uri = $file->getFileUri();
    $path = self::getFilePathRelativeToDrupalRoot($uri);
    $library_info = [
      // Load script in header.
      'header' => TRUE,
      'js' => [
        $path => [
          // Preprocess allows aggregation for better performance.
          'preprocess' => TRUE,
        ],
      ],
    ];
    return $library_info;
  }

  /**
   * Builds a file path for a given uri relative to the Drupal root directory.
   */
  private static function getFilePathRelativeToDrupalRoot($uri) {
    $path = \Drupal::service('file_url_generator')->generateAbsoluteString($uri);
    return str_replace(base_path(), '/', parse_url($path, PHP_URL_PATH));
  }

}
