<?php

namespace Drupal\pianoanalytics\Form;

use Drupal\Component\Utility\DeprecationHelper;
use Drupal\Core\Asset\LibraryDiscoveryCollector;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\FileRepositoryInterface;
use Drupal\pianoanalytics\PianoAnalyticsUtils;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure the Piano Analytics module.
 *
 * @package Drupal\pianoanalytics\Form
 */
class PianoAnalyticsSettingsForm extends ConfigFormBase {

  const UPLOAD_LOCATION = 'public://pianoanalytics/';

  const PIANOANALYTICS_SETTINGS = 'pianoanalytics.settings';

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The file repository service.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected FileRepositoryInterface $fileRepository;

  /**
   * Drupal\Core\Asset\LibraryDiscoveryCollector definition.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryCollector
   */
  protected $libraryDiscovery;

  /**
   * Constructs a new PianoAnalyticsSettingsForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\file\FileRepositoryInterface $file_repository
   *   The file repository service.
   * @param \Drupal\Core\Asset\LibraryDiscoveryCollector $library_discovery
   *   The library discovery service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typed_config_manager,
    EntityTypeManagerInterface $entity_type_manager,
    FileSystemInterface $file_system,
    FileRepositoryInterface $file_repository,
    LibraryDiscoveryCollector $library_discovery,
  ) {
    parent::__construct($config_factory, $typed_config_manager);
    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $file_system;
    $this->fileRepository = $file_repository;
    $this->libraryDiscovery = $library_discovery;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    $library_discovery_name = DeprecationHelper::backwardsCompatibleCall(
      \Drupal::VERSION, '11.1', fn() => 'library.discovery', fn() => 'library.discovery.collector'
    );
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('file.repository'),
      $container->get($library_discovery_name)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pianoanalytics_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::PIANOANALYTICS_SETTINGS];
  }

  /**
   * Gets the Piano Analytics configuration.
   *
   * @return \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   *   The Piano Analytics configuration object.
   */
  protected function getConfig() {
    return $this->config(self::PIANOANALYTICS_SETTINGS);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->getConfig();

    $form['pianoanalytics'] = [
      '#type' => 'details',
      '#title' => $this->t('Piano Analytics settings'),
      '#open' => TRUE,
      '#description' => $this->t('You can choose to use the internal generic Piano Analytics javascript file provided by the module, use an external URL (ex: CDN) or upload a custom javascript file.'),
    ];

    $form['pianoanalytics']['site'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Identifiant du site'),
      '#description' => $this->t("AT-Internet : Numéro de site"),
      '#default_value' => $config->get('site'),
      '#states' => [
        'required' => [
          ':input[name="mode"]' => ['value' => 'generic'],
        ],
      ],
    ];
    $form['pianoanalytics']['collect_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Serveur de collecte'),
      '#description' => $this->t("AT-Internet : Domaine de collecte des données"),
      '#default_value' => $config->get('collect_domain'),
      '#states' => [
        'required' => [
          ':input[name="mode"]' => ['value' => 'generic'],
        ],
      ],
    ];

    $form['pianoanalytics']['mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Piano Analytics file location'),
      '#description' => $this->t("Choose the way you want to handle the Piano Analytics script file."),
      '#options' => [
        'generic' => $this
          ->t("Default Piano Analytics file provided by the module"),
        'url' => $this
          ->t("Remote Piano Analytics URL"),
        'file' => $this
          ->t("Uploaded Piano Analytics file"),
        'manual' => $this
          ->t("I want to manage my piano-analytics.js by myself"),
      ],
      '#default_value' => $config->get('mode') ?? 'generic',
      '#required' => TRUE,
    ];

    $form['pianoanalytics']['pianoanalytics_file_container'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name="mode"]' => ['value' => 'file'],
        ],
      ],
    ];
    $pianoanalytics_file = $config->get('pianoanalytics_file');
    $form['pianoanalytics']['pianoanalytics_file_container']['pianoanalytics_file'] = [
      '#title' => $this->t('Piano Analytics file (with a .txt extension)'),
      '#type' => 'managed_file',
      '#description' => $this->t('Upload your AT Internet Piano Analytics file here. A .txt extension is required as Drupal will not allow .js files to be uploaded.'),
      '#default_value' => $pianoanalytics_file ? [$pianoanalytics_file] : NULL,
      '#upload_location' => self::UPLOAD_LOCATION,
      '#upload_validators' => [
        'file_validate_extensions' => ['txt'],
      ],
      // Sadly code below doesn't work, it's a well-known issue.
      // That's the reason we need to use a container to hide it.
      /*'#states' => [
        'visible' => [
          ':input[name="mode"]' => ['value' => 'file'],
        ],
        'required' => [
          ':input[name="mode"]' => ['value' => 'file'],
        ],
      ],*/
    ];

    $form['pianoanalytics']['pianoanalytics_url'] = [
      '#title' => $this->t('Piano Analytics URL'),
      '#type' => 'url',
      '#description' => $this->t('Paste the URL of your Piano Analytics file here.'),
      '#default_value' => $config->get('pianoanalytics_url'),
      '#states' => [
        'visible' => [
          ':input[name="mode"]' => ['value' => 'url'],
        ],
        'required' => [
          ':input[name="mode"]' => ['value' => 'url'],
        ],
      ],
    ];

    $form['label_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Libellé de la page'),
      '#description' => $this->t("Ce paramètre détermine comment seront intitulées les pages dans les tableaux de bord d'AT-Internet."),
      '#options' => [
        'page_title' => $this->t('Titre de la page (unicité non garantie)'),
        'path_alias' => $this->t('Chemin de la page (path alias unique)'),
      ],
      '#default_value' => $config->get('label_type') ?? 'path_alias',
    ];
    $form['chapters_from_breadcrumb'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Initialize page chapters from Drupal breadcrumb'),
      '#description' => $this->t("Ce paramètre permet de renseigner les pages de niveau 1, 2 et 3 à partir des éléments du fil d'ariane Drupal (breadcrumb)."),
      '#default_value' => $config->get('chapters_from_breadcrumb') ?? 0,
    ];
    $form['secure'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Secure cookie (https)'),
      '#description' => $this->t("AT-Internet : ce paramètre permet d'envoyer les informations de manière sécurisée ou non. Cocher dans le cas d'un protocole HTTPS et décocher pour un protocole HTTP."),
      '#default_value' => $config->get('secure') ?? 1,
    ];
    $form['cookie_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domaine des cookies'),
      '#description' => $this->t("AT-Internet : Domaine pour l'écriture des cookies. Si ce paramètre est vide (cookieDomain: ''), le domaine de la page courante sera utilisé."),
      '#default_value' => $config->get('cookie_domain'),
    ];
    $form['cnil_exempt'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Exemption CNIL"),
      '#description' => $this->t("AT-Internet : L'activation du mode hybride exempt de l'autorité CNIL permet une mesure restreinte par le Tracker. Seuls les paramètres jugés « strictement nécessaires » sont collectés."),
      '#default_value' => $config->get('cnil_exempt') ?? 1,
    ];
    $form['add_event_url'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Prise en compte de la query string"),
      '#description' => $this->t("AT-Internet : ajout de la query string à la propriété contenant l'url de la page"),
      '#default_value' => $config->get('add_event_url') ?? 1,
    ];

    // Link specific configurations.
    $form['linktracking'] = [
      '#type' => 'details',
      '#title' => $this->t('Links and downloads'),
      '#group' => 'tracking_scope',
    ];
    $form['linktracking']['trackoutbound'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track clicks on outbound links'),
      '#default_value' => $config->get('track.outbound') ?? 0,
    ];
    $form['linktracking']['trackmailto'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track clicks on mailto links'),
      '#default_value' => $config->get('track.mailto') ?? 0,
    ];
    $form['linktracking']['trackfiles'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track downloads (clicks on file links) for the following extensions'),
      '#default_value' => $config->get('track.files') ?? 0,
    ];
    $form['linktracking']['trackfiles_extensions'] = [
      '#title' => $this->t('List of download file extensions'),
      '#title_display' => 'invisible',
      '#type' => 'textfield',
      '#default_value' => $config->get('track.files_extensions') ?? '',
      '#description' => $this->t('A file extension list separated by the | character that will be tracked as download when clicked. Regular expressions are supported. For example: @extensions', ['@extensions' => PianoAnalyticsUtils::TRACKFILES_EXTENSIONS]),
      '#maxlength' => 500,
      '#states' => [
        'enabled' => [
          ':input[name="trackfiles"]' => ['checked' => TRUE],
        ],
        // Note: Form required marker is not visible as title is invisible.
        'required' => [
          ':input[name="trackfiles"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->getConfig();

    $mode = $form_state->getValue('mode');
    $library = $this->getPianoAnalyticsLibrary($mode);

    $sdk_file = $this->generateSdkInitScriptFile($form_state);
    $pa_file = $this->processScriptFile($form_state);

    $config
      ->set('mode', $mode)
      ->set('pianoanalytics_library', $library)
      ->set('pianoanalytics_url', $form_state->getValue('pianoanalytics_url'))
      ->set('pianoanalytics_file', isset($pa_file) ? $pa_file->id() : NULL)
      ->set('sdk_init_file', isset($sdk_file) ? $sdk_file->id() : NULL)
      ->set('site', $form_state->getValue('site'))
      ->set('collect_domain', $form_state->getValue('collect_domain'))
      ->set('label_type', $form_state->getValue('label_type'))
      ->set('chapters_from_breadcrumb', $form_state->getValue('chapters_from_breadcrumb'))
      ->set('secure', $form_state->getValue('secure'))
      ->set('cookie_domain', $form_state->getValue('cookie_domain'))
      ->set('cnil_exempt', $form_state->getValue('cnil_exempt'))
      ->set('add_event_url', $form_state->getValue('add_event_url'))
      ->set('track.outbound', $form_state->getValue('trackoutbound'))
      ->set('track.mailto', $form_state->getValue('trackmailto'))
      ->set('track.files', $form_state->getValue('trackfiles'))
      ->set('track.files_extensions', $form_state->getValue('trackfiles_extensions'))
      ->save();

    $this->libraryDiscovery->clear();

    parent::submitForm($form, $form_state);
  }

  /**
   * Generates the Piano Analytics init script file.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The Piano Analytics settings form state.
   *
   * @return \Drupal\file\FileInterface|null
   *   The generated script file or null if generation failed.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function generateSdkInitScriptFile(FormStateInterface $form_state) {
    $sdk_init_script = $this->generateSdkInitScript($form_state);
    $pa_directory = self::UPLOAD_LOCATION;
    $sdk_init_uri = $pa_directory . $this->getSdkInitFilename();
    /** @var \Drupal\Core\File\FileSystemInterface $file_system */
    if ($this->fileSystem->prepareDirectory($pa_directory,
      FileSystemInterface:: CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
      /** @var \Drupal\file\FileRepositoryInterface $file_repository */
      $sdk_init_file = $this->fileRepository->writeData(
        $sdk_init_script, $sdk_init_uri, FileExists::Replace);
      if (!$sdk_init_file->isPermanent()) {
        // We don't want the Piano Analytics SDK file to be garbage collected.
        $sdk_init_file->setPermanent();
        $sdk_init_file->save();
      }
      return $sdk_init_file;
    }
    else {
      // @todo log error.
      return NULL;
    }
  }

  /**
   * Returns the Piano Analytics init script file name.
   */
  protected function getSdkInitFilename() {
    return 'pianoanalytics-sdk-init.js';
  }

  /**
   * Generates the Piano Analytics init script.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The Piano Analytics settings form state.
   *
   * @return string
   *   The generated Piano Analytics init script.
   */
  protected function generateSdkInitScript(FormStateInterface $form_state) {
    $sdk_init_script = "window._pac = window._pac || {};";
    $unsecure = $form_state->getValue('secure') === '0';
    if ($unsecure) {
      $sdk_init_script .= "\n_pac.cookieSecure = false;";
    }
    $cookie_domain = $form_state->getValue('cookie_domain');
    if (!empty($cookie_domain)) {
      $sdk_init_script .= "\n_pac.cookieDomain = '" . $cookie_domain . "';";
    }
    return $sdk_init_script;
  }

  /**
   * Generates the Piano Analytics script file.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The Piano Analytics settings form state.
   *
   * @return \Drupal\file\FileInterface|null
   *   The generated Piano Analytics script file.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function processScriptFile(FormStateInterface $form_state) {
    $file_value = $form_state->getValue('pianoanalytics_file');
    if (empty($file_value) || !is_array($file_value)) {
      return NULL;
    }
    $fid = $file_value[0];
    if (empty($fid)) {
      return NULL;
    }
    /** @var \Drupal\file\FileInterface $file */
    $file = $this->entityTypeManager->getStorage('file')->load($fid);
    if (empty($file)) {
      return NULL;
    }
    // Rename newly uploaded file by moving or replacing it.
    $tagTargetUri = self::UPLOAD_LOCATION . $this->getPianoAnalyticsScriptFilename();
    if ($file->getFileUri() === $tagTargetUri) {
      return $file;
    }
    else {
      $moved_file = $this->fileRepository->move(
        $file, $tagTargetUri, FileExists::Replace);
      $moved_file->setFilename($this->getPianoAnalyticsScriptFilename());
      // We don't want the Piano Analytics file to be garbage collected.
      $moved_file->setPermanent();
      $moved_file->save();
      return $moved_file;
    }
  }

  /**
   * Returns the Piano Analytics script file name.
   */
  protected function getPianoAnalyticsScriptFilename() {
    return 'piano-analytics.js';
  }

  /**
   * Gets the library name from selected configuration mode.
   *
   * @param string $mode
   *   The Piano Analytics library mode from configuration.
   *
   * @return string
   *   The corresponding Piano Analytics library name.
   */
  protected function getPianoAnalyticsLibrary(string $mode) {
    switch ($mode) {
      case 'generic':
        $library = 'pianoanalytics/pianoanalytics_js';
        break;

      case 'url':
        $library = 'pianoanalytics/pianoanalytics_url';
        break;

      case 'file':
        $library = 'pianoanalytics/pianoanalytics_file';
        break;

      default:
        $library = '';
    }
    return $library;
  }

}
