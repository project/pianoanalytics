# Piano Analytics

AT Internet Piano Analytics integration for Drupal.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/pianoanalytics).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/pianoanalytics).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module.
For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at Administration > Extend.
2. Go to `/admin/config/system/pianoanalytics/settings` to configure
the module settings.


## Maintainers

- Frank Mably - [mably](https://www.drupal.org/u/mably)
